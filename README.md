[dump]: ../../src/master/dump.sql "SQL dump"
[csv]: ../../src/master/reviews.csv "Review CSV"

# README

## Description
This project is a PSE pre-trial challenge that consists of a hotel review analyzer.

## Install instructions
1. Use the SQL [dump][] to create a new DB with the necessary tables and data.
2. Clone this project.
3. Open a terminal, go to the project folder and execute  `composer install`.
4. Run the project with `php app/console server:run`.

## Use instructions
The interface is self-explanatory, just go to *Add review* to add a new review, *Review management* to see and delete reviews, and so on for each category.
You can use this [csv][] to upload it and review all the reviews at once.