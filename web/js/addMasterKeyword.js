$('#add-master-keyword-form').submit(function(ev) {
  ev.preventDefault();
  formdata = $('#add-master-keyword-form').serialize();
  console.log('Form data: ' + formdata);
  $.ajax({
    type: "POST",
    url: '/addMasterKeyword',
    data: formdata ,
    async: true,
    dataType: "json",
    success: function(response) {
      console.log('Added master keyword: ' + JSON.stringify(response));
      if ( $("#form_masterKeyword option[value='" + response.id + "']").val() !== undefined) {
        $('#form_masterKeyword').val(response.id);
        return;
      }

      var option = {
        value: response.id,
        text: response.name
      }
      $('#form_masterKeyword').append('<option value="' + response.id + '">' + response.name + '</option>').val(response.id);
      console.log('Option added: ' + JSON.stringify(option));
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown);
    },
  });
});