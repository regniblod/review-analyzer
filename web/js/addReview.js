var table;

$(document).ready(function() {
  table = $('#keywordTable').DataTable();
});

$('#add-review-form').submit(function(ev) {
  ev.preventDefault();
  formdata = $('#add-review-form').serialize();
  console.log('Form data: ' + formdata);
  $.ajax({
    type: "POST",
    url: '/addReview',
    data: formdata ,
    async: true,
    dataType: "json",
    success: function(response) {
      console.log('Raw response: ' + response);
        addRow(response);
        $('#form_text').val('');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown);
    },
  });
});

$('#add-review-csv-form').submit(function(ev) {
  ev.preventDefault();
  $.ajax({
    type: "POST",
    url: '/addReviewCsv',
    data: new FormData( this ),
    processData: false,
    contentType: false,
    async: true,
    success: function(response) {
      console.log('Raw response: ' + response);
      for (var i = 0; i < response.length; i++) {
        addRow(response[i]);
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown);
    },
  });
});

function addRow(review) {
  console.log('Added review: ' + JSON.stringify(review));
  var keywordAdjectives = '';
  for (var i = review.review_details.length - 1; i >= 0; i--) {
    var keyword = review.review_details[i].keyword_adjective.master_keyword.name;
    var adjective = review.review_details[i].keyword_adjective.adjective.name;
    var score = review.review_details[i].keyword_adjective.adjective.score;
    keywordAdjectives += keyword + '->' + adjective + '[' + score + "], ";
  };
  table.row.add([
    review.id,
    review.text,
    keywordAdjectives,
    review.score
  ]).draw();
  table.columns.adjust();
}