var table;

$(document).ready(function() {
  table = $('#table').DataTable();
  table.columns.adjust();
});

function addRemoveEvents(route) {
  $('table').on('click', 'button', function() {
    var item = $(this);
    var id = $(this).attr('id');
    $.ajax({
      type: "POST",
      url: '/' + route + '/' + id,
      async: true,
      dataType: "json",
      success: function(response) {
        if (response.success) {
          table
          .row(item.parents('tr'))
          .remove()
          .draw();
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        console.log('Error: ' + errorThrown);
      },
    });
  });
}