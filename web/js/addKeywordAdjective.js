var table;

$(document).ready(function() {
  table = $('#keywordAdjectiveTable').DataTable();
});

$('#add-keyword-adjective-form').submit(function(ev) {
  ev.preventDefault();
  // TODO: serialize this.
  formdata = $('#add-keyword-adjective-form').serialize();
  console.log('Form data: ' + formdata);
  $.ajax({
    type: "POST",
    url: '/addKeywordAdjective',
    data: formdata ,
    async: true,
    dataType: "json",
    success: function(response) {
      console.log('Added keyword-adjective relationship: ' + JSON.stringify(response));
      table.row.add([
        response.id,
        response.master_keyword.name,
        response.adjective.name
      ]).draw();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log('Error: ' + errorThrown);
    },
  });
});