<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="adjective")
 */
class Adjective
{
    /**
     * @ORM\ManyToOne(targetEntity="AdjectiveType", inversedBy="adjectives")
     * @JMS\Exclude
     */
    protected $adjectiveType;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $name;

    /**
     * @ORM\Column(name="adjective_type_id", type="integer")
     * @JMS\Exclude
     */
    protected $adjectiveTypeId;

    /**
     * @ORM\Column(name="score", type="integer")
     */
    protected $score;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Adjective
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adjectiveTypeId
     *
     * @param integer $adjectiveTypeId
     * @return Adjective
     */
    public function setAdjectiveTypeId($adjectiveTypeId)
    {
        $this->adjectiveTypeId = $adjectiveTypeId;

        return $this;
    }

    /**
     * Get adjectiveTypeId
     *
     * @return integer
     */
    public function getAdjectiveTypeId()
    {
        return $this->adjectiveTypeId;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Adjective
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set adjectiveType
     *
     * @param \AppBundle\Entity\AdjectiveType $adjectiveType
     * @return Adjective
     */
    public function setAdjectiveType(\AppBundle\Entity\AdjectiveType $adjectiveType = null)
    {
        $this->adjectiveType = $adjectiveType;

        return $this;
    }

    /**
     * Get adjectiveType
     *
     * @return \AppBundle\Entity\AdjectiveType
     */
    public function getAdjectiveType()
    {
        return $this->adjectiveType;
    }
}
