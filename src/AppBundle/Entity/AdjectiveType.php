<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="adjective_type")
 */
class AdjectiveType
{
    /**
     * @ORM\OneToMany(targetEntity="Adjective", mappedBy="adjectiveType")
     */
    protected $adjectives;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $type;

    public function __construct()
    {
        $this->adjectives = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return AdjectiveType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add adjectives
     *
     * @param \AppBundle\Entity\Adjective $adjectives
     * @return AdjectiveType
     */
    public function addAdjective(\AppBundle\Entity\Adjective $adjectives)
    {
        $this->adjectives[] = $adjectives;

        return $this;
    }

    /**
     * Remove adjectives
     *
     * @param \AppBundle\Entity\Adjective $adjectives
     */
    public function removeAdjective(\AppBundle\Entity\Adjective $adjectives)
    {
        $this->adjectives->removeElement($adjectives);
    }

    /**
     * Get adjectives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdjectives()
    {
        return $this->adjectives;
    }
}
