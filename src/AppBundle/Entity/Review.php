<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="review")
 */
class Review
{
    /**
     * @ORM\OneToMany(targetEntity="ReviewDetails", mappedBy="review")
     */
    protected $reviewDetails;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=2048)
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     */
    private $score = 0;

    public function __construct()
    {
        $this->keywords = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Review
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Review
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Add reviewDetails
     *
     * @param \AppBundle\Entity\ReviewDetails $reviewDetails
     * @return Review
     */
    public function addReviewDetail(\AppBundle\Entity\ReviewDetails $reviewDetails)
    {
        $this->reviewDetails[] = $reviewDetails;

        return $this;
    }

    /**
     * Remove reviewDetails
     *
     * @param \AppBundle\Entity\ReviewDetails $reviewDetails
     */
    public function removeReviewDetail(\AppBundle\Entity\ReviewDetails $reviewDetails)
    {
        $this->reviewDetails->removeElement($reviewDetails);
    }

    /**
     * Get reviewDetails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewDetails()
    {
        return $this->reviewDetails;
    }
}
