<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="keyword")
 */
class Keyword
{
    /**
     * @ORM\ManyToOne(targetEntity="MasterKeyword", inversedBy="keywords")
     * @ORM\JoinColumn(name="master_keyword_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $masterKeyword;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $name;

    /**
     * @ORM\Column(name="master_keyword_id", type="integer")
     */
    protected $masterKeywordId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Keyword
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set masterKeywordId
     *
     * @param integer $masterKeywordId
     * @return Keyword
     */
    public function setMasterKeywordId($masterKeywordId)
    {
        $this->masterKeywordId = $masterKeywordId;

        return $this;
    }

    /**
     * Get masterKeywordId
     *
     * @return integer
     */
    public function getMasterKeywordId()
    {
        return $this->masterKeywordId;
    }

    /**
     * Set masterKeyword
     *
     * @param \AppBundle\Entity\MasterKeyword $masterKeyword
     * @return Keyword
     */
    public function setMasterKeyword(\AppBundle\Entity\MasterKeyword $masterKeyword = null)
    {
        $this->masterKeyword = $masterKeyword;

        return $this;
    }

    /**
     * Get masterKeyword
     *
     * @return \AppBundle\Entity\MasterKeyword
     */
    public function getMasterKeyword()
    {
        return $this->masterKeyword;
    }
}
