<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="master_keyword")
 */
class MasterKeyword
{
    /**
     * @ORM\OneToMany(targetEntity="Keyword", mappedBy="masterKeyword")
     * @JMS\Exclude
     */
    protected $keywords;

    /**
     * @ORM\OneToMany(targetEntity="KeywordAdjective", mappedBy="masterKeyword")
     * @JMS\Exclude
     */
    protected $keywordAdjectives;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    public function __construct()
    {
        $this->keywords = new ArrayCollection();
        $this->adjectives = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MasterKeyword
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add keywords
     *
     * @param \AppBundle\Entity\Keyword $keywords
     * @return MasterKeyword
     */
    public function addKeyword(\AppBundle\Entity\Keyword $keywords)
    {
        $this->keywords[] = $keywords;

        return $this;
    }

    /**
     * Remove keywords
     *
     * @param \AppBundle\Entity\Keyword $keywords
     */
    public function removeKeyword(\AppBundle\Entity\Keyword $keywords)
    {
        $this->keywords->removeElement($keywords);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Add keywordAdjectives
     *
     * @param \AppBundle\Entity\KeywordAdjective $keywordAdjectives
     * @return MasterKeyword
     */
    public function addKeywordAdjective(\AppBundle\Entity\KeywordAdjective $keywordAdjectives)
    {
        $this->keywordAdjectives[] = $keywordAdjectives;

        return $this;
    }

    /**
     * Remove keywordAdjectives
     *
     * @param \AppBundle\Entity\KeywordAdjective $keywordAdjectives
     */
    public function removeKeywordAdjective(\AppBundle\Entity\KeywordAdjective $keywordAdjectives)
    {
        $this->keywordAdjectives->removeElement($keywordAdjectives);
    }

    /**
     * Get keywordAdjectives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeywordAdjectives()
    {
        return $this->keywordAdjectives;
    }
}
