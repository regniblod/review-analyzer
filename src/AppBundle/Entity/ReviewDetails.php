<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="review_details")
 */
class ReviewDetails
{
    /**
     * @ORM\ManyToOne(targetEntity="Review", inversedBy="reviewDetails")
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id", onDelete="CASCADE")
     * @JMS\Exclude
     */
    protected $review;

    /**
     * @ORM\ManyToOne(targetEntity="KeywordAdjective")
     * @ORM\JoinColumn(name="keyword_adjective_id", referencedColumnName="id")
     */
    protected $keywordAdjective;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     */
    protected $id;

    /**
     * @ORM\Column(name="review_id", type="integer")
     * @JMS\Exclude
     */
    protected $reviewId;

    /**
     * @ORM\Column(name="keyword_adjective_id", type="integer")
     * @JMS\Exclude
     */
     private $keywordAdjectiveId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reviewId
     *
     * @param integer $reviewId
     * @return ReviewDetails
     */
    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;

        return $this;
    }

    /**
     * Get reviewId
     *
     * @return integer
     */
    public function getReviewId()
    {
        return $this->reviewId;
    }

    /**
     * Set keywordAdjectiveId
     *
     * @param integer $keywordAdjectiveId
     * @return ReviewDetails
     */
    public function setKeywordAdjectiveId($keywordAdjectiveId)
    {
        $this->keywordAdjectiveId = $keywordAdjectiveId;

        return $this;
    }

    /**
     * Get keywordAdjectiveId
     *
     * @return integer
     */
    public function getKeywordAdjectiveId()
    {
        return $this->keywordAdjectiveId;
    }

    /**
     * Set review
     *
     * @param \AppBundle\Entity\Review $review
     * @return ReviewDetails
     */
    public function setReview(\AppBundle\Entity\Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \AppBundle\Entity\Review
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set keywordAdjective
     *
     * @param \AppBundle\Entity\KeywordAdjective $keywordAdjective
     * @return ReviewDetails
     */
    public function setKeywordAdjective(\AppBundle\Entity\KeywordAdjective $keywordAdjective = null)
    {
        $this->keywordAdjective = $keywordAdjective;

        return $this;
    }

    /**
     * Get keywordAdjective
     *
     * @return \AppBundle\Entity\KeywordAdjective
     */
    public function getKeywordAdjective()
    {
        return $this->keywordAdjective;
    }
}
