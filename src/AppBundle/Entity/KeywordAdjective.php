<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="keyword_adjective")
 */
class KeywordAdjective
{
    /**
     * @ORM\ManyToOne(targetEntity="MasterKeyword", inversedBy="keywordAdjectives")
     * @ORM\JoinColumn(name="master_keyword_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $masterKeyword;

    /**
     * @ORM\ManyToOne(targetEntity="Adjective")
     * @ORM\JoinColumn(name="adjective_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $adjective;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="master_keyword_id", type="integer")
     * @JMS\Exclude
     */
    protected $masterKeywordId;

    /**
     * @ORM\Column(name="adjective_id", type="integer")
     * @JMS\Exclude
     */
    protected $adjectiveId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set masterKeywordId
     *
     * @param integer $masterKeywordId
     * @return KeywordAdjective
     */
    public function setMasterKeywordId($masterKeywordId)
    {
        $this->masterKeywordId = $masterKeywordId;

        return $this;
    }

    /**
     * Get masterKeywordId
     *
     * @return integer
     */
    public function getMasterKeywordId()
    {
        return $this->masterKeywordId;
    }

    /**
     * Set adjectiveId
     *
     * @param integer $adjectiveId
     * @return KeywordAdjective
     */
    public function setAdjectiveId($adjectiveId)
    {
        $this->adjectiveId = $adjectiveId;

        return $this;
    }

    /**
     * Get adjectiveId
     *
     * @return integer
     */
    public function getAdjectiveId()
    {
        return $this->adjectiveId;
    }

    /**
     * Set masterKeyword
     *
     * @param \AppBundle\Entity\MasterKeyword $masterKeyword
     * @return KeywordAdjective
     */
    public function setMasterKeyword(\AppBundle\Entity\MasterKeyword $masterKeyword = null)
    {
        $this->masterKeyword = $masterKeyword;

        return $this;
    }

    /**
     * Get masterKeyword
     *
     * @return \AppBundle\Entity\MasterKeyword
     */
    public function getMasterKeyword()
    {
        return $this->masterKeyword;
    }

    /**
     * Set adjective
     *
     * @param \AppBundle\Entity\Adjective $adjective
     * @return KeywordAdjective
     */
    public function setAdjective(\AppBundle\Entity\Adjective $adjective = null)
    {
        $this->adjective = $adjective;

        return $this;
    }

    /**
     * Get adjective
     *
     * @return \AppBundle\Entity\Adjective
     */
    public function getAdjective()
    {
        return $this->adjective;
    }
}
