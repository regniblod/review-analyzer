<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Review;
use AppBundle\Entity\ReviewDetails;

class ReviewController extends Controller
{
    /**
     * @Route("/manageReviews", name="manageReviwes")
     * @Route("/", name="home")
     * @Template("manage_reviews.html.twig")
     */
    public function manageReviewsAction()
    {
        $reviews = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->findAll();

        return ['reviews' => $reviews];
    }

    /**
     * @Route("/addReview", name="addReview")
     * @Template("add_review.html.twig")
     */
    public function addReviewdAction(Request $request)
    {
        $review = new Review();

        $reviewForm = $this->createFormBuilder($review)
            ->add('text', 'textarea')
            ->add('save', 'submit', ['label' => 'Add review'])
            ->getForm();

        $reviewForm->handleRequest($request);

        if ($reviewForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $review = $this->analyzeReview($review);
            $em->flush();

            // refresh review to get ID and reviewDetails.
            $em->refresh($review);

            if ($request->isXmlHttpRequest()) {
                $serializer = $this->get('jms_serializer');
                $serializedReview = $serializer->serialize($review, 'json');
                return new JsonResponse(json_decode($serializedReview));
            }
        }

        return $this->render('add_review.html.twig', [
            'reviewForm' => $reviewForm->createView()
        ]);
    }

    /**
     * @Route("/addReviewCsv", name="addReviewCsv")
     * @Template()
     */
    public function addReviewCsvAction(Request $request)
    {
        $csvForm = $this->get("form.factory")->createNamedBuilder("csvForm")
            ->add('csv', 'file')
            ->add('send', 'submit', ['label' => 'Send CSV'])
            ->getForm();

        $csvForm->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($csvForm->isValid()) {
                $csvPath = $csvForm->get('csv')->getData()->getRealPath();

                $csvData = file_get_contents($csvPath);
                $csvData = str_getcsv(str_replace("\n", '', $csvData), ';');
                $csvData = array_filter($csvData);

                $em = $this->getDoctrine()->getManager();
                $analizedReviews = [];
                foreach ($csvData as $reviewText) {
                    $review = new Review();
                    $review->setText($reviewText);
                    $review = $this->analyzeReview($review);
                    $em->persist($review);
                    $analizedReviews[] = $review;
                }

                $em->flush();

                $responseReviews;
                $serializer = $this->get('jms_serializer');
                foreach ($analizedReviews as $analizedReview) {
                    $em->refresh($analizedReview);
                    $responseReviews[] = json_decode($serializer->serialize($analizedReview, 'json'));
                }

                return new JsonResponse($responseReviews);
            }
        }

        return $this->render('add_review_csv.html.twig', [
            'csvForm' => $csvForm->createView()
        ]);
    }

    /**
     * @Route("/deleteReview/{id}", name="deleteReview")
     */
    public function deleteReviewAction($id)
    {
        $review = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->findOneById($id);

        if (!$review) {
            return new JsonResponse(['success' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($review);
        $em->flush();

        return new JsonResponse(['success' => true]);

    }

    private function analyzeReview(Review $review)
    {
        $em = $this->getDoctrine()->getManager();

        $keywords = $this->getDoctrine()
        ->getRepository('AppBundle:Keyword')
        ->findAll();

        $delimiters = ['.', ',', '&'];
        $sentences = explode('.', str_replace($delimiters, $delimiters[0], $review->getText()));

        // iterate over each sentence to calculate each one score.
        foreach ($sentences as $sentence) {

            // don't process empty sentence.
            if (!$sentence) {
                continue;
            }

            $words = explode(' ', $sentence);
            $word = null;

            // find keyword in sentence
            foreach ($words as $word) {
                $keyword = null;
                foreach ($keywords as $keyword) {
                    if (strtolower($word) == strtolower($keyword->getName())) {

                        // get all adjectives of a master keyword.
                        $keywordAdjectives = $keyword->getMasterKeyword()->getKeywordAdjectives();
                        $keywordAdjective = null;

                        // find adjective in sentence
                        foreach ($keywordAdjectives as $keywordAdjective) {
                            if (strpos(strtolower($sentence), strtolower($keywordAdjective->getAdjective()->getName())) !== false) {
                                // save review details
                                $reviewDetails = new ReviewDetails();
                                $reviewDetails->setReview($review);
                                $reviewDetails->setKeywordAdjective($keywordAdjective);
                                $em->persist($reviewDetails);

                                $review->setScore($review->getScore() + $keywordAdjective->getAdjective()->getScore());

                                break;
                            }
                        }
                    }
                }
            }

        }

        return $review;
    }

}