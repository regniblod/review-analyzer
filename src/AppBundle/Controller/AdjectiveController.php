<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Adjective;

class AdjectiveController extends Controller
{
    /**
     * @Route("/manageAdjectives", name="manageAdjectives")
     * @Template("manage_adjectives.html.twig")
     */
    public function manageAdjectivesAction()
    {
        $adjectives = $this->getDoctrine()
            ->getRepository('AppBundle:Adjective')
            ->findAll();

        return ['adjectives' => $adjectives];
    }

    /**
     * @Route("/addAdjective", name="addAdjective")
     * @Template("add_adjective.html.twig")
     */
    public function addAdjectiveAction(Request $request)
    {
        $adjective = new Adjective();

        $adjectiveForm = $this->createFormBuilder($adjective)
            ->add('adjectiveType', 'entity', [
            'class'        => 'AppBundle:AdjectiveType',
            'choice_label' => 'type'
            ])
            ->add('name')
            ->add('score')
            ->add('save', 'submit', ['label' => 'Add adjective'])
            ->getForm();

        $adjectiveForm->handleRequest($request);

        if ($adjectiveForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($adjective);
            $em->flush();

            return $this->redirectToRoute('manageAdjectives');
        }

        return $this->render('add_adjective.html.twig', [
            'adjectiveForm' => $adjectiveForm->createView()
        ]);
    }

    /**
     * @Route("/deleteAdjective/{id}", name="deleteAdjective")
     */
    public function deleteAdjectiveAction($id)
    {
        $adjective = $this->getDoctrine()
            ->getRepository('AppBundle:Adjective')
            ->findOneById($id);

        if (!$adjective) {
            return new JsonResponse(['success' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($adjective);
        $em->flush();

        return new JsonResponse(['success' => true]);

    }

}