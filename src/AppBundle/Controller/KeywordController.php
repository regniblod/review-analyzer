<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Keyword;
use AppBundle\Entity\MasterKeyword;

class KeywordController extends Controller
{
    /**
     * @Route("/manageKeywords", name="manageKeywords")
     * @Template("manage_keywords.html.twig")
     */
    public function manageKeywordsAction()
    {
        $keywords = $this->getDoctrine()
            ->getRepository('AppBundle:Keyword')
            ->findAll();

        return ['keywords' => $keywords];
    }

    /**
     * @Route("/addKeyword", name="addKeyword")
     * @Template("add_keyword.html.twig")
     */
    public function addKeywordAction(Request $request)
    {
        $keyword = new Keyword();

        $keywordForm = $this->createFormBuilder($keyword)
            ->add('masterKeyword', 'entity', [
                'class'        => 'AppBundle:MasterKeyword',
                'choice_label' => 'name'
                ])
            ->add('name')
            ->add('save', 'submit', ['label' => 'Add keyword'])
            ->getForm();

        $keywordForm->handleRequest($request);

        if ($keywordForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $existentKeyword = $em->getRepository('AppBundle:Keyword')->findOneBy([
                'masterKeyword' => $keyword->getMasterKeyword(),
                'name'          => $keyword->getName()
            ]);

            if (!$existentKeyword) {
                $em->persist($keyword);
                $em->flush();
            }

            return $this->redirectToRoute('manageKeywords');
        }

        return $this->render('add_keyword.html.twig', [
            'keywordForm' => $keywordForm->createView()
        ]);
    }

    /**
     * @Route("/addMasterKeyword", name="addMasterKeyword")
     * @Template("add_master_keyword.html.twig")
     */
    public function addMasterKeywordAction(Request $request)
    {
        $masterKeyword = new MasterKeyword();

        $masterKeywordForm = $this->createFormBuilder($masterKeyword)
            ->add('name')
            ->add('save', 'submit', ['label' => '+'])
            ->getForm();

        $masterKeywordForm->handleRequest($request);

        if ($masterKeywordForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $existentMasterKeyword = $em->getRepository('AppBundle:MasterKeyword')->findOneBy([
                'name' => $masterKeyword->getName()
            ]);

            if (!$existentMasterKeyword) {
                $em->persist($masterKeyword);
                $em->flush();
                $em->refresh($masterKeyword);
            } else {
                $masterKeyword = $existentMasterKeyword;
            }

            if ($request->isXmlHttpRequest()) {
                $serializer = $this->get('jms_serializer');
                $masterKeyword = $serializer->serialize($masterKeyword, 'json');
                return new JsonResponse(json_decode($masterKeyword));
            }
        }

        return $this->render('add_master_keyword.html.twig', [
            'masterKeywordForm' => $masterKeywordForm->createView()
        ]);
    }

    /**
     * @Route("/deleteKeyword/{id}", name="deleteKeyword")
     */
    public function deleteKeywordAction($id)
    {
        $keyword = $this->getDoctrine()
            ->getRepository('AppBundle:Keyword')
            ->findOneById($id);

        if (!$keyword) {
            return new JsonResponse(['success' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($keyword);
        $em->flush();

        return new JsonResponse(['success' => true]);

    }

}