<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\KeywordAdjective;

class KeywordAdjectiveController extends Controller
{
    /**
     * @Route("/manageKeywordAdjectives", name="manageKeywordAdjectives")
     * @Template("manage_keyword_adjectives.html.twig")
     */
    public function manageAKeyworddjectivesAction()
    {
        $keywordAdjectives = $this->getDoctrine()
            ->getRepository('AppBundle:KeywordAdjective')
            ->findAll();

        return ['keywordAdjectives' => $keywordAdjectives];
    }

    /**
     * @Route("/addKeywordAdjective", name="addKeywordAdjective")
     * @Template("add_keyword_adjective.html.twig")
     */
    public function addKeywordAdjectiveAction(Request $request)
    {
        $keywordAdjective = new KeywordAdjective();

        $form = $this->createFormBuilder($keywordAdjective)
            ->add('masterKeyword', 'entity', [
                'class'        => 'AppBundle:MasterKeyword',
                'choice_label' => 'name'
            ])
            ->add('adjective', 'entity', [
                'class'        => 'AppBundle:Adjective',
                'choice_label' => 'name'
            ])
            ->add('save', 'submit', ['label' => 'Add keyword-adjective relationship'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $existentKeywordAdjective = $em->getRepository('AppBundle:KeywordAdjective')->findBy([
                'masterKeyword' => $keywordAdjective->getMasterKeyword(),
                'adjective'     => $keywordAdjective->getAdjective()
            ]);

            if (!$existentKeywordAdjective) {
                $em->persist($keywordAdjective);
                $em->flush();
            } else {
                $keywordAdjective = $existentKeywordAdjective[0];
            }

            if ($request->isXmlHttpRequest()) {
                $serializer = $this->get('jms_serializer');
                $serializedKeywordAdjective = $serializer->serialize($keywordAdjective, 'json');
                return new JsonResponse(json_decode($serializedKeywordAdjective));
            }
        }

        return $this->render('add_keyword_adjective.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/deleteKeywordAdjective/{id}", name="deleteKeywordAdjective")
     */
    public function deleteKeywordAdjectiveAction($id)
    {
        $keywordAdjective = $this->getDoctrine()
            ->getRepository('AppBundle:KeywordAdjective')
            ->findOneById($id);

        if (!$keywordAdjective) {
            return new JsonResponse(['success' => false]);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($keywordAdjective);
        $em->flush();

        return new JsonResponse(['success' => true]);

    }

}